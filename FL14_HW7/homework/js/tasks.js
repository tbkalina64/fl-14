'use strict';

const arr = [1, 2, 3, 4, 56, 7, 8, 76, 5, 241, 5, 356, 567,2];
function maxElement(arr) {
  return Math.max.apply(null, arr);
}

const array = [1, 2, 3];
const copiedArray = [...array]

const addUniqueId = (obj) => {
    const newObj = Object.create(obj);
    newObj[Symbol('id')] = 'id';
    return newObj;
}
const oldObj = {
    name: 'Someone',
    details: {
      id: 1,
      age: 11,
      university: 'UNI'
    }
  }
  function regroupObj({name: firstName, details: {id, age, university}}) {
    return {
        university,
        user: {
            age,
            firstName,
            id
        }
    }
}

const array1 = [1, 1, 23, 3, 4, 5, 6, 5, 4, 23,2, 1, 1, 1, 1, 1];
const findUniqueElements = array1 => {
  return array1.filter((value, index, self) => {
    return self.indexOf(value) === index;
  });
};
findUniqueElements(array1);

const hide = '6789';
function hideNumber(){
    return hide.padStart(10, '*');
}
hideNumber();

function add(param1, param2) {
    return param1 + param2;
  }
add(1);


function fetchUserData(){
    fetch('https://jsonplaceholder.typicode.com/users/')
        .then(response => response.json())
        .then(users => {
            users.forEach(function(user) {
                console.log(user.name);
            });
      
        });
}
fetchUserData();