function renderCalcButton() {
    let html = '<button onclick="promtExpression();">Run calculator</button>';
    html += '<div id="result"></div>';
    document.write(html);
 };
 
 function promtExpression() {
    let exp = prompt("Please input calculator expression");
    let result = calcExpression(exp);
    document.querySelector('#result').innerHTML = result;
 }
 
 function calcExpression(exp) {
    let result = '';
    try {
       result = eval(exp);
       if (!Number.isFinite(result)) {
          throw new Error('Invalid result');
       }
    } catch (e) {
       result = 'Your expression "' + exp + '" is invalid';
    }
    return result;
 }
 
 renderCalcButton();