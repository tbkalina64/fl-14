function visitLink(path) {
	let count = localStorage.getItem(path);
	count = (null === count) ? 1 : ++count;
	localStorage.setItem(path, count);
 }
 
 function viewResults() {
	let html = '<ul id="result">';
	for(let i=0; i<localStorage.length; i++) {
	   let key = localStorage.key(i);
	   html += `<li>You visited ${key} ${localStorage.getItem(key)} time(s)</li>`;
	}
	html += '</ul>';
 
	let block = document.querySelector('#result')
	if (block) {
	   block.remove();
	} 

	document.querySelector('body').innerHTML += html;
 
	localStorage.clear();
 }