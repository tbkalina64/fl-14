function findMiddleCharacter() {

    const string = prompt('Enter your word')
    let position;
    let length;

    if (string === /^\s*$/.test() || string === null) {
        alert('invalid data');
    } else {
        if (string.length % 2 === 1) {
            position = string.length / 2;
            length = 1;
        } else {
            position = string.length / 2 - 1;
            length = 2;
            if (string[0] === string[1]) {
                alert('Middle characters are the same');
            }
        }
        return string.substring(position, position + length);
    }
}
alert(findMiddleCharacter());
