function countIt() {
    const bat = document.getElementById('all-batteries').value;
    let defRate = document.getElementById('defective-batteries').value;
    const text1 = 'Amount of batteries:';
    const text2 = 'Defective rate (%):';
    const text3 = 'Amount of defective batteries:';
    const text4 = 'Amount of working batteries:';
    const persentage = 100;
    const even = 2;
    let workBatteries;
    let defectiveBatteries;

        if (bat.replace(/\s/g, '').length === 0 || isNaN(bat) || bat <= 0) {
            if (defRate.replace(/\s/g, '').length === 0 || isNaN(defRate) || 0 <= defRate || defRate <= persentage){
                alert('Invalid input data'); 
            }
        } else {
        workBatteries = bat * defRate / persentage;
        defectiveBatteries = bat - workBatteries;

        alert(`${text1} ${bat}
        ${text2} ${defRate} 
        ${text3} ${workBatteries.toFixed(even)} 
        ${text4} ${defectiveBatteries.toFixed(even)}`);
    }
}
document.getElementById('count').addEventListener('click', countIt);
