// task 1
function getAge(birthdate) {
    let now = new Date();
    let years = now.getFullYear() - birthdate.getFullYear();
    birthdate.setFullYear(now.getFullYear());
    if (years > 0 && birthdate < now) {
        years--;
    } 
 
    return years; 
 }
 
 // Task 2
 function getWeekDay(date) {
   let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
   return weekdays[date.getDay()];
 }
 
 // Task 3
 function getProgrammersDay(year) {
    let date = new Date(year, 0, 256);
    return getWeekDay(date);
 }
 
 // Task 4
 function howFarIs(weekday) {
     weekday = weekday.toLowerCase();
     let date = new Date();
     if (weekday === getWeekDay(date).toLowerCase()) {
        return `Hey, today is ${ weekday } =)`;
     }
 
     for (let i = 1; i <= 7; i++) {
        date.setDate(date.getDate() + 1);
        if (weekday === getWeekDay(date).toLowerCase()) {
            return `It is ${ i } day(s) left till ${ weekday }`;
        }
     }
 
     return 'Invalid weekday name';    
 }
 
 // Test 5
 function isValidIdentifier(id) {
   return null !== id.match(/^[a-z_$][a-z0-9_$]*$/i);
 }
 
 // Test 6
 function capitalize(str) {
    let result = [];
    str.split(/ /g).forEach(w => result.push(w[0].toUpperCase() + w.substring(1)));
    return result.join(' ');
 }
 
 // Test 7
 function isValidAudioFile(fname) {
    return null !== fname.match(/^[a-z]+\.(mp3|flac|alac|aac)$/i);
 }
 
 // Test 8
 function getHexadecimalColors(str) {
    let result = [];
    let m = str.match(/#[0-9a-f]{3,}/ig);
    if (null !== m) {
       for (let i = 0; i < m.length; i++) {
          if (4 === m[i].length || 7 === m[i].length) {
             result.push(m[i]);
          }
       }
    }
    return result;
 }
 
 // Test 9
 function isValidPassword(pass) {
    return !!(pass.match(/.{8,}/) && pass.match(/[a-z]/) && pass.match(/[A-Z]/) && pass.match(/[0-9]/));
 }
 
 // Test 10
 function addThousandsSeparators(s) {
    s = s.split('').reverse().join('');
    s = s.replace(/([0-9]{3})/g, '$1,');
    return s.split('').reverse().join('').replace('^,', '');
 }