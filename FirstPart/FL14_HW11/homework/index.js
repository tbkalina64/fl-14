
// Task #1
function isEquals(a, b) {
    return a === b;
}

// Task #2
function numberToString(num) {
    return num.toString();
}

// Task #3
function storeNames() {
    let names = [];

    for (let i = 0; i < arguments.length; i++) {
        names.push(arguments[i]);
    }

    return names;
}

// Task #4
function getDivision(a, b) {
    if (isBigger(b, a)) {
        let tmp = a;
        a = b;
        b = tmp;
    }

    return a / b;
}

function isBigger(num1, num2) {
    return num1 > num2;
}


// Task #5
function negativeCount(nums) {
    let count = 0;
    nums.forEach(function(n) {
        if (n < 0) {
            count++
        }
    });

    return count;
}

// Task #6
function letterCount(haystack, needle) {
    let count = 0;
    let start = 0;
    let lenNeedle = needle.length;

    while (haystack.length >= start) {
        let index = haystack.indexOf(needle, start);
        if (-1 === index) {
            break;
        }
        start = index + lenNeedle;
        count++;
    }

    return count;
}

// Task #7
function countPoints(games) {
    let points = 0;
    let winScores = 3;
    let drawScores = 1;

    games.forEach(function(game) {
        let score = game.split(':');
        let scoreX = parseInt(score[0]);
        let scoreY = parseInt(score[1]);
        
        if (isBigger(scoreX, scoreY)) {
            points += winScores;
        } else if (scoreX === scoreY) {
            points += drawScores;
        }
    });

    return points;
}
