document.getElementById('fetchUserDataBtn').addEventListener('click', fetchUserData);


function fetchUserData() {
    fetch('https://jsonplaceholder.typicode.com/users/')
        .then(
            response => response.json()
        )
        .then(users => {
            let output = '';

            users.forEach(function (user) {
                output += `
                            <tr> 
                            <td class="userId">
                                ${user.id}
                            </td>
                            <td class="userName">
                                ${user.name}
                            </td>
                            <td class="userMail">
                                ${user.email}
                            </td>
                            <td class="userPhone">
                                ${user.phone}
                            </td>
                            <td>
                            <button id="editUser">EDIT</button>
                            <button id="deleteUser">DELETE</button>
</td>
                            </tr>
                        `;
            });
            output += '</tr>'
            document.getElementById('response').innerHTML = output;
})
       .catch(error => alert(error));

}
